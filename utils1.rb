require 'uri'

module Tire
  module Utils
    def unescape(s)
      s = s.to_s.respond_to?(:force_encoding) ? s.to_s.force_encoding(Encoding::UTF_8) : s.to_s
      URI.decode_www_form(s)
    end
    extend self
  end
end
